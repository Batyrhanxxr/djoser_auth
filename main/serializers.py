from unicodedata import category
from rest_framework import serializers
from .models import *


class CategorySerializer(serializers.ModelSerializer):
    product = serializers.SlugRelatedField(slug_field='name', read_only=True)
    class Meta:
        model = Category
        fields = '__all__'


class ProductSerializer(serializers.ModelSerializer):
    category =  CategorySerializer()
    class Meta:
        model = Product
        fields = 'id name image description price created updated category'.split()

