from django.urls import path
from . import views

urlpatterns = [
    path('products/', views.ProductListAPIView.as_view()),
    path('products/<int:id>/', views.ProductDetailAPIView.as_view()),
    path('category/', views.CategoryListAPIView.as_view()),
    path('category/<int:pk>/', views.CategoryProductListAPIView.as_view()),



] 